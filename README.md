# Virtual Wallet

Aplicação frontend de carteira virtual ~ Bitcoin & Brita

## Requisitos

- [Node](https://nodejs.org/en/) >= 12.0.0 (Dê preferência a uma versão **LTS**)
- [NPM](https://docs.npmjs.com/cli/npm) >= 6.4.1
- [Yarn](https://yarnpkg.com/lang/en/) >= 1.15.2
- [Gitflow](http://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html#instalacao) >= 1.12.3

## Demo

A aplicação está disponível para acesso aqui: [Heroku > Virtual Wallet](https://virtual-wallet-frontend.herokuapp.com/login)

## Instalação

Use o [yarn](https://yarnpkg.com/lang/en/) para instalar as dependências do repositório.

```bash
  $ yarn
```

## Tech Stack

- [React](https://pt-br.reactjs.org/)
  - [React Hooks](https://pt-br.reactjs.org/docs/hooks-intro.html)
  - [Redux](https://redux.js.org/)
  - [Styled Components](https://www.styled-components.com/)

## Desenvolvimento

Utilize o seguinte comando para inicializar a aplicação:

```
  $ yarn start
```

A aplicação estará disponível em: `http://localhost:3000/login`

## Commit

Para commitar, utilize o commando:

```
  $ yarn commit
```

## Ajuda

**Possui alguma dúvida?**

Entre em contato comigo, [clicando aqui](mailto:diego.bmss+virtualwallet@gmail.com)