import styled from 'styled-components';

export const Title = styled.h1`
  font-family: var(--font-family-second);
  font-size: 2.6rem;

  font-size: ${({ size }) => (size === 'large' ? '2.6rem' : '1.6rem')};

  color: var(--color-primary);
`;
