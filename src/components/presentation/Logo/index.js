import React from 'react';

import * as S from './styled';

const Logo = ({ size = 'small' }) => (
  <S.Title size={size}>Virtual Wallet</S.Title>
);

export default Logo;
