// import React, { useMemo, useEffect } from "react";
// import { useHistory } from "react-router-dom";
// import { useForm } from "react-hook-form";
// import { toast } from 'react-toastify';
// import * as Yup from "yup";

// import { useSelector, useDispatch } from 'react-redux';
// import { AuthActions } from '../../../store/ducks/auth';
// import {
//   REQUEST_PENDING,
//   REQUEST_REJECTED,
//   REQUEST_RESOLVED,
// } from "../../../utils/constants";

// import Button from '../../../components/core/Button';
// import Container from '../../../components/core/Container';
// import { Input } from '../../../components/core/Form';
// import Logo from '../../../components/presentation/Logo';

// import { useYupValidationResolver } from '../../../helpers';

// import * as S from "./styled";

// const SignUp = () => {
//   const history = useHistory();
//   const dispatch = useDispatch();
//   const { signUp } = useSelector((state) => state.auth);

//   useEffect(() => {
//     if(signUp.requestStatus === REQUEST_RESOLVED) {
//       toast.success("Usuário cadastrado com sucesso.")
//       dispatch(AuthActions.signUpClean());
//       setTimeout(() => {
//         history.push('/login');
//       }, 3000)
//     } else if(signUp.requestStatus === REQUEST_REJECTED) {
//       toast.error(signUp.requestResponse)
//       dispatch(AuthActions.signUpClean());
//     }
//   }, [dispatch, history, signUp]);

//   const validationSchema = useMemo(
//     () =>
//       Yup.object({
//         name: Yup.string().required("Campo obrigatório"),
//         email: Yup
//           .string()
//           .email("E-mail inválido")
//           .required("Campo obrigatório"),
//         password: Yup.string().required("Campo obrigatório"),
//       }),
//     []
//   );

//   const resolver = useYupValidationResolver(validationSchema);
//   const { handleSubmit, register, errors } = useForm({ resolver });
//   const onSubmit = (values) => {
//     dispatch(AuthActions.signUpRequest(values));
//   };

//   return (
//     <S.Wrapper>
//       <Container>
//         <S.Content>
//           <Logo size="large">Virtual Wallet</Logo>
//           <S.Form onSubmit={handleSubmit(onSubmit)}>
//             <Input
//               label="Nome"
//               name="name"
//               type="name"
//               placeholder="Seu nome"
//               reference={register}
//               error={errors.name && errors.name.message}
//               disabled={false}
//             />

//             <Input
//               label="E-mail"
//               name="email"
//               type="email"
//               placeholder="Seu e-mail"
//               reference={register}
//               error={errors.email && errors.email.message}
//               disabled={false}
//             />

//             <Input
//               label="Senha"
//               name="password"
//               type="password"
//               placeholder="Sua senha"
//               reference={register}
//               error={errors.password && errors.password.message}
//               disabled={false}
//             />

//             <Button type="submit" isLoading={signUp.requestStatus === REQUEST_PENDING}>Acessar</Button>

//             <S.Link to="/login">Já tenho login</S.Link>
//           </S.Form>
//         </S.Content>
//       </Container>
//     </S.Wrapper>
//   );
// };

// export default SignUp;

import React, { useMemo, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as Yup from 'yup';

import { useSelector, useDispatch } from 'react-redux';
import { UsersActions } from '../../../store/ducks/users';
import {
  REQUEST_PENDING,
  REQUEST_REJECTED,
  REQUEST_RESOLVED,
} from '../../../utils/constants';

import Button from '../../core/Button';
import Container from '../../core/Container';
import { Input } from '../../core/Form';
import Logo from '../Logo';

import { useYupValidationResolver } from '../../../helpers';

import * as S from './styled';

const SignUp = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.users);

  useEffect(() => {
    if (users.requestStatus === REQUEST_RESOLVED) {
      toast.success('Usuário cadastrado com sucesso.');
      dispatch(UsersActions.cleanCreateUser());
      setTimeout(() => {
        history.push('/login');
      }, 3000);
    } else if (users.requestStatus === REQUEST_REJECTED) {
      toast.error(users.requestResponse);
      dispatch(UsersActions.cleanCreateUser());
    }
  }, [dispatch, history, users]);

  const validationSchema = useMemo(
    () => Yup.object({
      name: Yup.string().required('Campo obrigatório'),
      email: Yup
        .string()
        .email('E-mail inválido')
        .required('Campo obrigatório'),
      password: Yup.string().required('Campo obrigatório'),
    }),
    [],
  );

  const resolver = useYupValidationResolver(validationSchema);
  const { handleSubmit, register, errors } = useForm({ resolver });
  const onSubmit = (values) => {
    dispatch(UsersActions.createUserRequest(values));
  };

  return (
    <S.Wrapper>
      <Container>
        <S.Content>
          <Logo size="large">Virtual Wallet</Logo>
          <S.Form onSubmit={handleSubmit(onSubmit)}>
            <Input
              label="Nome"
              name="name"
              type="name"
              placeholder="Seu nome"
              reference={register}
              error={errors.name && errors.name.message}
              disabled={false}
            />

            <Input
              label="E-mail"
              name="email"
              type="email"
              placeholder="Seu e-mail"
              reference={register}
              error={errors.email && errors.email.message}
              disabled={false}
            />

            <Input
              label="Senha"
              name="password"
              type="password"
              placeholder="Sua senha"
              reference={register}
              error={errors.password && errors.password.message}
              disabled={false}
            />

            <Button type="submit" isLoading={users.requestStatus === REQUEST_PENDING}>Acessar</Button>

            <S.Link to="/login">Já tenho login</S.Link>
          </S.Form>
        </S.Content>
      </Container>
    </S.Wrapper>
  );
};

export default SignUp;
