import styled from 'styled-components';

import { Link as ReactLink } from 'react-router-dom';
import { Form as ComponentForm } from '../../core/Form';

export const Wrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  width: 100%;
  max-width: 315px;
  text-align: center;
`;

export const Form = styled(ComponentForm)`
  margin-top: 30px;
`;

export const Link = styled(ReactLink)`
  color: var(--color-white-1);
  margin-top: 15px;
  font-size: 16px;
  opacity: 0.8;

  &:hover {
    opacity: 1;
  }
`;
