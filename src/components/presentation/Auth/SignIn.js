// import React, { useMemo, useEffect } from "react";
// import { useForm } from "react-hook-form";
// import { toast } from 'react-toastify';
// import * as Yup from "yup";

// import { useSelector, useDispatch } from 'react-redux';
// import { AuthActions } from '../../../store/ducks/auth';
// import {
//   REQUEST_PENDING,
//   REQUEST_REJECTED,
//   REQUEST_RESOLVED,
// } from "../../../utils/constants";

// import Button from '../../../components/core/Button';
// import Container from '../../../components/core/Container';
// import { Input } from '../../../components/core/Form';
// import Logo from '../../../components/presentation/Logo';

// import { useYupValidationResolver } from '../../../helpers';

// import * as S from "./styled";

// const SignIn = () => {
//   const dispatch = useDispatch();
//   const { signIn } = useSelector((state) => state.auth);

// useEffect(() => {
//   if(signIn.requestStatus === REQUEST_RESOLVED) {
//     dispatch(AuthActions.signInClean());
//   } else if(signIn.requestStatus === REQUEST_REJECTED) {
//     toast.error(signIn.requestResponse);
//     dispatch(AuthActions.signInClean());
//   }
// }, [dispatch, signIn]);

//   const validationSchema = useMemo(
//     () =>
//       Yup.object({
//         email: Yup
//           .string()
//           .email("E-mail inválido")
//           .required("Campo obrigatório"),
//         password: Yup.string().required("Campo obrigatório"),
//       }),
//     []
//   );

//   const resolver = useYupValidationResolver(validationSchema);
//   const { handleSubmit, register, errors } = useForm({ resolver });
//   const onSubmit = (values) => {
//     dispatch(AuthActions.auth(values));
//   };

//   return (
//     <S.Wrapper>
//       <Container>
//         <S.Content>
//           <Logo size="large">Virtual Wallet</Logo>
//           <S.Form onSubmit={handleSubmit(onSubmit)}>
//             <Input
//               label="E-mail"
//               name="email"
//               type="email"
//               placeholder="Seu e-mail"
//               reference={register}
//               error={errors.email && errors.email.message}
//               disabled={false}
//             />

//             <Input
//               label="Senha"
//               name="password"
//               type="password"
//               placeholder="Sua senha"
//               reference={register}
//               error={errors.password && errors.password.message}
//               disabled={false}
//             />

//             <Button type="submit" isLoading={signIn.requestStatus === REQUEST_PENDING}>Acessar</Button>

//             <S.Link to="/register">Criar conta</S.Link>
//           </S.Form>
//         </S.Content>
//       </Container>
//     </S.Wrapper>
//   );
// };

// export default SignIn;

import React, { useMemo, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as Yup from 'yup';

import { useSelector, useDispatch } from 'react-redux';
import { UsersActions } from '../../../store/ducks/users';
import {
  REQUEST_PENDING,
  REQUEST_REJECTED,
  REQUEST_RESOLVED,
} from '../../../utils/constants';

import Button from '../../core/Button';
import Container from '../../core/Container';
import { Input } from '../../core/Form';
import Logo from '../Logo';

import { useYupValidationResolver } from '../../../helpers';

import * as S from './styled';

const SignIn = () => {
  const dispatch = useDispatch();
  const { auth } = useSelector((state) => state.users);

  useEffect(() => {
    if (auth.requestStatus === REQUEST_RESOLVED) {
      dispatch(UsersActions.cleanAuthLogin());
    } else if (auth.requestStatus === REQUEST_REJECTED) {
      toast.error(auth.requestResponse);
      dispatch(UsersActions.cleanAuthLogin());
    }
  }, [auth.requestResponse, auth.requestStatus, dispatch]);

  const validationSchema = useMemo(
    () => Yup.object({
      email: Yup
        .string()
        .email('E-mail inválido')
        .required('Campo obrigatório'),
      password: Yup.string().required('Campo obrigatório'),
    }),
    [],
  );

  const resolver = useYupValidationResolver(validationSchema);
  const { handleSubmit, register, errors } = useForm({ resolver });
  const onSubmit = (values) => {
    dispatch(UsersActions.authLoginRequest(values));
  };

  return (
    <S.Wrapper>
      <Container>
        <S.Content>
          <Logo size="large">Virtual Wallet</Logo>
          <S.Form onSubmit={handleSubmit(onSubmit)}>
            <Input
              label="E-mail"
              name="email"
              type="email"
              placeholder="Seu e-mail"
              reference={register}
              error={errors.email && errors.email.message}
              disabled={false}
            />

            <Input
              label="Senha"
              name="password"
              type="password"
              placeholder="Sua senha"
              reference={register}
              error={errors.password && errors.password.message}
              disabled={false}
            />

            <Button type="submit" isLoading={auth.requestStatus === REQUEST_PENDING}>Acessar</Button>

            <S.Link to="/register">Criar conta</S.Link>
          </S.Form>
        </S.Content>
      </Container>
    </S.Wrapper>
  );
};

export default SignIn;
