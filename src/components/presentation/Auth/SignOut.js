import React, { useEffect } from 'react';

import { useDispatch } from 'react-redux';
import { UsersActions } from '../../../store/ducks/users';

const SignOut = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(UsersActions.authLogoutRequest());
  }, [dispatch]);

  return (
    <></>
  );
};

export default SignOut;
