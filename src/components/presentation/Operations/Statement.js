import React from 'react';

import { useSelector } from 'react-redux';

import Container from '../../core/Container';

import * as S from './styled';

const Statement = () => {
  const { statements, authenticated } = useSelector((state) => state.users);

  return (
    <Container>
      <S.Container>
        <S.Title>Extrato de Compra e Venda</S.Title>
        <S.Card>
          {statements.filter((x) => x.user.key === authenticated.key && x.operation !== 'change').length > 0 ? (
            <S.Table>
              <thead>
                <tr>
                  <th>Data</th>
                  <th>Operação</th>
                  <th>Moeda</th>
                  <th>Valor</th>
                </tr>
              </thead>
              <tbody>
                {(statements.filter((x) => x.user.key === authenticated.key && x.operation !== 'change')).map((item) => (
                  <tr>
                    <td>{item.date}</td>
                    <td>{item.operation === 'sell' ? 'Venda' : (item.operation === 'buy' ? 'Compra' : '')}</td>
                    <td>{item.coin === 'brita' ? 'Brita' : (item.coin === 'bitcoin' ? 'Bitcoin' : '')}</td>
                    <td>{item.value}</td>
                  </tr>
                ))}
              </tbody>
            </S.Table>
          ) : (
            <S.Error>Nenhuma transação registrada até o momento.</S.Error>
          )}
        </S.Card>
      </S.Container>

      <S.Container>
        <S.Title>Extrato de Troca</S.Title>
        <S.Card>
          {statements.filter((x) => x.user.key === authenticated.key && x.operation === 'change').length > 0 ? (
            <S.Table>
              <thead>
                <tr>
                  <th>Data</th>
                  <th>Operação</th>
                  <th>Moeda de Origem</th>
                  <th>Valor de Origem</th>
                  <th>Moeda de Destino</th>
                  <th>Valor de Destino</th>
                </tr>
              </thead>
              <tbody>
                {(statements.filter((x) => x.user.key === authenticated.key && x.operation === 'change')).map((item) => (
                  <tr>
                    <td>{item.date}</td>
                    <td>Troca</td>
                    <td>{item.coinChange === 'brita' ? 'Brita' : (item.coinChange === 'bitcoin' ? 'Bitcoin' : '')}</td>
                    <td>{item.value}</td>
                    <td>{item.coin === 'brita' ? 'Brita' : (item.coin === 'bitcoin' ? 'Bitcoin' : '')}</td>
                    <td>{item.newValue}</td>
                  </tr>
                ))}
              </tbody>
            </S.Table>
          ) : (
            <S.Error>Nenhuma transação registrada até o momento.</S.Error>
          )}
        </S.Card>
      </S.Container>
    </Container>
  );
};

export default Statement;
