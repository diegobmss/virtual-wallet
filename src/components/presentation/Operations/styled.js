import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 45px;
  margin-bottom: 45px;
`;

export const Title = styled.h1`
  font-family: var(--font-family-second);
  color: var(--color-white-1);
  margin-bottom: 15px;
`;

export const Balance = styled.div`
  h2, h3 {
    color: var(--color-black-1);
  }

  h2 {
    margin-bottom: 15px;
  }

  h3 {
    font-weight: normal;

    span {
      font-weight: bold;
    }
  }

  padding-bottom: 30px;
  border-bottom: 1px solid var(--color-gray-3);
  margin-bottom: 30px;
`;

export const Card = styled.div`
  box-shadow: 0 2px 2px 0 var(--color-black-4), 0 3px 1px -2px var(--color-black-5), 0 1px 5px 0 var(--color-black-6);
  background: var(--color-white-1);
  border-radius: 4px;
  padding: 20px;
`;

export const Table = styled.table`
  width: 100%;
  text-align: left;
  border-collapse: collapse;

  thead {
    border-top: 1px solid var(--color-gray-3);
  }

  td, th {
    padding: 20px;
    border-bottom: 1px solid var(--color-gray-3);
  }
`;

export const Error = styled.h3`
  font-weight: normal;
  color: var(--color-black-1);
`;
