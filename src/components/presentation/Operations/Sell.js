import React, { useMemo, useEffect } from 'react';
import Skeleton from 'react-loading-skeleton';

import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as Yup from 'yup';

import { useSelector, useDispatch } from 'react-redux';
import { CoinsActions } from '../../../store/ducks/coins';
import { UsersActions } from '../../../store/ducks/users';
import { REQUEST_PENDING, REQUEST_RESOLVED } from '../../../utils/constants';

import Button from '../../../components/core/Button';
import Container from '../../../components/core/Container';
import { Form, Input, Select } from '../../../components/core/Form';

import { useYupValidationResolver } from '../../../helpers';

import * as S from './styled';

const Sell = () => {
  const dispatch = useDispatch();
  const { bitcoin, brita } = useSelector((state) => state.coins);
  const { authenticated, listUsers, sellCoin } = useSelector(
    (state) => state.users
  );

  useEffect(() => {
    dispatch(CoinsActions.bitcoinRequest());
    dispatch(CoinsActions.britaRequest());
  }, [dispatch]);

  const validationSchema = useMemo(
    () =>
      Yup.object({
        coin: Yup.string().required('Campo obrigatório'),
        value: Yup.string().required('Campo obrigatório'),
      }),
    []
  );

  const resolver = useYupValidationResolver(validationSchema);
  const { handleSubmit, register, errors } = useForm({ resolver });
  const onSubmit = (values) => {
    if (values.value > listUsers[authenticated.key][values.coin]) {
      return toast.error(
        'Transação negada! O valor informado é acima do saldo disponível.'
      );
    }

    if (values.value === '0') {
      return toast.error('Transação negada! Informe um valor maior que zero.');
    }

    values.prices = {
      bitcoin: bitcoin.requestResponse?.ticker?.buy,
      brita: brita.requestResponse?.value[0]?.cotacaoCompra,
    };

    dispatch(UsersActions.sellCoinRequest(values));
  };

  return (
    <Container>
      <S.Container>
        <S.Title>Vender</S.Title>
        <S.Card>
          <S.Balance>
            <h2>Saldo</h2>
            <h3>
              Reais: <span>R$ {listUsers[authenticated.key].reais}</span>
            </h3>
            <h3>
              Bitcoin: <span>{listUsers[authenticated.key].bitcoin}</span>
            </h3>
            <h3>
              Brita: <span>{listUsers[authenticated.key].brita}</span>
            </h3>
          </S.Balance>

          <S.Balance>
            <h2>Cotação atual</h2>
            <h3>
              Bitcoin:{' '}
              <span>
                {bitcoin.requestStatus === REQUEST_PENDING ? (
                  <Skeleton />
                ) : bitcoin.requestStatus === REQUEST_RESOLVED ? (
                  bitcoin.requestResponse?.ticker?.buy
                ) : (
                  'Erro ao obter a cotação'
                )}
              </span>
            </h3>
            <h3>
              Brita:{' '}
              <span>
                {brita.requestStatus === REQUEST_PENDING ? (
                  <Skeleton />
                ) : brita.requestStatus === REQUEST_RESOLVED ? (
                  brita.requestResponse?.value &&
                  brita.requestResponse?.value[0]?.cotacaoCompra
                ) : (
                  'Erro ao obter a cotação'
                )}
              </span>
            </h3>
          </S.Balance>

          <Form onSubmit={handleSubmit(onSubmit)}>
            <Select
              label="Quero vender"
              name="coin"
              reference={register}
              error={errors.coin && errors.coin.message}
              disabled={false}
              theme="light"
              options={[
                {
                  text: 'Bitcoin',
                  id: '1',
                  value: 'bitcoin',
                },
                {
                  text: 'Brita',
                  id: '2',
                  value: 'brita',
                },
              ]}
            />

            <Input
              label="Quantidade"
              name="value"
              type="number"
              step="any"
              placeholder="0"
              reference={register}
              error={errors.value && errors.value.message}
              disabled={false}
              theme="light"
            />

            <Button
              type="submit"
              isLoading={sellCoin.requestStatus === REQUEST_PENDING}
            >
              Vender
            </Button>
          </Form>
        </S.Card>
      </S.Container>
    </Container>
  );
};

export default Sell;
