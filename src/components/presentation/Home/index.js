import React, { useEffect } from 'react';
import Skeleton from 'react-loading-skeleton';

import { useSelector, useDispatch } from 'react-redux';
import { CoinsActions } from '../../../store/ducks/coins';
import { REQUEST_PENDING, REQUEST_RESOLVED } from '../../../utils/constants';

import Container from '../../../components/core/Container';

import * as S from './styled';

const Home = () => {
  const dispatch = useDispatch();
  const { bitcoin, brita } = useSelector((state) => state.coins);
  const { authenticated, listUsers } = useSelector((state) => state.users);

  useEffect(() => {
    dispatch(CoinsActions.bitcoinRequest());
    dispatch(CoinsActions.britaRequest());
  }, [dispatch]);

  return (
    <Container>
      <S.Container>
        <S.Title>Olá, {authenticated.name}!</S.Title>
        <S.Grid>
          <S.ItemGrid>
            <S.Card>
              <S.Coin>Saldo atual</S.Coin>
              <S.Price>Reais: R$ {listUsers[authenticated.key].reais}</S.Price>
              <S.Price>Bitcoin: {listUsers[authenticated.key].bitcoin}</S.Price>
              <S.Price>Brita: {listUsers[authenticated.key].brita}</S.Price>
            </S.Card>
          </S.ItemGrid>
        </S.Grid>
      </S.Container>
      <S.Container>
        <S.Title>Cotações:</S.Title>
        <S.Grid>
          <S.ItemGrid>
            <S.Card>
              <S.Coin>Bitcoin</S.Coin>
              <S.Price>
                {bitcoin.requestStatus === REQUEST_PENDING ? (
                  <Skeleton />
                ) : bitcoin.requestStatus === REQUEST_RESOLVED ? (
                  bitcoin.requestResponse?.ticker?.sell
                ) : (
                  'Erro ao obter a cotação'
                )}
              </S.Price>
            </S.Card>
          </S.ItemGrid>
          <S.ItemGrid>
            <S.Card>
              <S.Coin>Brita</S.Coin>
              <S.Price>
                {brita.requestStatus === REQUEST_PENDING ? (
                  <Skeleton />
                ) : brita.requestStatus === REQUEST_RESOLVED ? (
                  brita.requestResponse?.value &&
                  brita.requestResponse?.value[0]?.cotacaoVenda
                ) : (
                  'Erro ao obter a cotação'
                )}
              </S.Price>
            </S.Card>
          </S.ItemGrid>
        </S.Grid>
      </S.Container>
    </Container>
  );
};

export default Home;
