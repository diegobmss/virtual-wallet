import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 45px;
  margin-bottom: 45px;
`;

export const Title = styled.h1`
  font-family: var(--font-family-second);
  color: var(--color-white-1);
  margin-bottom: 15px;
`;

export const Grid = styled.ul`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 20px;
  list-style: none;
`;

export const ItemGrid = styled.li`
  display: flex;
  flex-direction: column;
`;

export const Card = styled.div`
  box-shadow: 0 2px 2px 0 var(--color-black-4), 0 3px 1px -2px var(--color-black-5), 0 1px 5px 0 var(--color-black-6);
  background: var(--color-primary);
  border-radius: 4px;
  padding: 20px;
`;

export const Coin = styled.h1`
  font-family: var(--font-family-second);
  color: var(--color-white-1);
`;

export const Price = styled.h4`
  color: var(--color-white-1);
`;
