import React from 'react';
import { Link } from 'react-router-dom';
import { MdArrowForward } from 'react-icons/md';

import Container from '../../core/Container';
import { Flex } from '../../core/Grid';
import Logo from '../Logo';

import * as S from './styled';

const Header = () => (
  <S.Navbar>
    <Container>
      <Flex direction="row" verticalAlign="space-between" horizontalAlign="center">
        <Link to="/">
          <Logo size="small" />
        </Link>
        <S.Menu>
          <Link to="/buy">
            Comprar
          </Link>
          <Link to="/sell">
            Vender
          </Link>
          <Link to="/change">
            Trocar
          </Link>
          <Link to="/statement">
            Extrato
          </Link>
          <Link to="/logout">
            Sair
            {' '}
            <MdArrowForward />
          </Link>
        </S.Menu>
      </Flex>
    </Container>
  </S.Navbar>
);

export default Header;
