import styled from 'styled-components';

export const Navbar = styled.div`
  box-shadow: var(--color-black-3) 0px 7px 12px;
  padding: 16px 0px;
  background: var(--color-primary-bg);
`;

export const Menu = styled.div`
  color: var(--color-white-1);

  a {
    padding-left: 15px;
    padding-right: 15px;

    svg {
      margin-left: 5px;
      vertical-align: -2px;
    }
  }
`;
