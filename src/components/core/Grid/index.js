import styled from "styled-components";
import media from "styled-media-query";

export const Flex = styled.div.attrs(
  ({ verticalAlign, horizontalAlign, direction, marginBottom }) => ({
    verticalAlign: verticalAlign ?? "flex-start",
    horizontalAlign: horizontalAlign ?? "flex-start",
    direction: direction ?? "column",
    marginBottom: marginBottom ?? "unset",
    marginLastChild: marginBottom ?? "2.4rem",
  })
)`
  display: flex;
  justify-content: ${({ verticalAlign }) => verticalAlign};
  align-items: ${({ horizontalAlign }) => horizontalAlign};
  flex-direction: ${({ direction }) => direction};
  margin-bottom: ${({ marginBottom }) => marginBottom};
  height: 100%;
  max-height: 100%;
  width: 100%;

  &:not(:last-child) {
    margin-bottom: ${({ marginLastChild }) => marginLastChild};
  }

  & > svg {
    margin-right: .4rem;
  }

  ${media.lessThan("large")`
    justify-content: flex-start;
  `}
`;