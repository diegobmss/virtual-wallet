import styled from 'styled-components';

export const Form = styled.form`
  height: auto;
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const Label = styled.label.attrs(({ theme }) => ({
  labelColor: theme === 'light' ? 'var(--color-black-1)' : 'var(--color-white-1)',
}))`
  color: ${({ labelColor }) => labelColor};
  font-size: 14px;
  font-weight: bold;
  line-height: 21px;
  margin-bottom: 4px;
`;

export const Error = styled.span`
  min-height: 14px;
  margin-top: 3px;
  font-size: 12px;
  color: var(--color-danger);
  align-self: flex-end;
`;

export const Input = styled.input`
  width: 100%;
  background: var(--color-white-1);
  border: 1px solid var(--color-white-2);
  border-radius: 4px;
  height: 44px;
  padding: 0 15px;
  color: var(--color-black-2);

  &::placeholder {
    color: var(--color-gray-2);
  }

  &:disabled {
    cursor: not-allowed;
    color: var(--color-gray-1);
  }
`;

export const Select = styled.select`
  width: 100%;
  background: var(--color-white-1);
  border: 1px solid var(--color-white-2);
  border-radius: 4px;
  height: 44px;
  padding: 0 15px;
  color: var(--color-black-2);

  &::placeholder {
    color: var(--color-gray-2);
  }

  &:disabled {
    cursor: not-allowed;
    color: var(--color-gray-1);
  }
`;
