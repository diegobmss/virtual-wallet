import React from 'react';

import { Flex } from '../Grid';

import * as S from './styled';

export const Form = ({ children, ...props }) => <S.Form {...props}>{children}</S.Form>;

export const Input = ({
  label,
  name,
  type,
  placeholder,
  reference,
  error,
  disabled,
  theme,
  ...props
}) => (
  <Flex direction="column" marginBottom="0.625rem">
    {label && <S.Label htmlFor={name} theme={theme}>{label}</S.Label>}
    <S.Input
      name={name}
      type={type}
      placeholder={placeholder}
      ref={reference}
      error={error}
      disabled={disabled}
      {...props}
    />
    <S.Error>{error}</S.Error>
  </Flex>
);

export const Select = ({
  label,
  name,
  reference,
  error,
  disabled,
  options,
  theme,
  ...props
}) => (
  <Flex direction="column" marginBottom="0.625rem">
    {label && <S.Label htmlFor={name} theme={theme}>{label}</S.Label>}
    <S.Select
      name={name}
      ref={reference}
      error={error}
      disabled={disabled}
      {...props}
    >
      <option key="0" value="">
        Selecione uma opção
      </option>
      {options.map(({ text, id, value }) => (
        <option key={id} value={value}>
          {text}
        </option>
      ))}
    </S.Select>
    <S.Error>{error}</S.Error>
  </Flex>
);
