import styled from 'styled-components';

export const Button = styled.button`
  margin: 5px 0 0;
  height: 44px;
  background: var(--color-button);
  font-weight: bold;
  color: var(--color-white-1);
  border: 0;
  border-radius: 4px;
  font-size: 16px;
  transition: background 0.2s;

  &:hover {
    background: var(--color-button-hover);
  }
`;

export const Loading = styled.div`
  border: 2px solid;
  border-color: var(--color-white-1);
  border-top: 2px solid;
  border-top-color: transparent;
  border-radius: 50%;
  width: 16px;
  height: 16px;
  animation: spin 2s linear infinite;
  margin: 0 auto;

  @keyframes spin {
    to {
      transform: rotate(360deg);
    }
  }
`;
