import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Button = ({ type, isLoading, children }) => (
  <S.Button type={type} isLoading={isLoading}>
    {isLoading ? <S.Loading /> : <>{children}</>}
  </S.Button>
);

Button.defaultProps = {
  isLoading: false,
};

Button.propTypes = {
  type: PropTypes.string.isRequired,
  isLoading: PropTypes.bool,
};

export default Button;
