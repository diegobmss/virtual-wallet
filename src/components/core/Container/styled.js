import styled from 'styled-components';

export const Container = styled.div`
  margin: 0 auto;
  max-width: var(--max-width-xs);

  @media screen and (min-width: 768px) {
    max-width: var(--max-width-sm);
  };

  @media screen and (min-width: 992px) {
    max-width: var(--max-width-md);
  };

  @media screen and (min-width: 1182px) {
    max-width: var(--max-width-lg);
  };
`;
