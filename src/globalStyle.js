import { createGlobalStyle } from 'styled-components';
import { darken } from 'polished';

import 'react-toastify/dist/ReactToastify.css';

import bg from './assets/images/bg.svg';

const GlobalStyle = createGlobalStyle`
  *,
  *::after,
  *::before {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  *:focus {
    outline: 0;
  }

  :root {
    --color-primary: #4CAF50;
    --color-primary-bg: #191920;
    --color-button: #4CAF50;
    --color-button-hover: ${darken(0.03, '#4CAF50')};
    --color-danger: #ea3328;
    --color-white-1: #ffffff;
    --color-white-2: rgb(204, 204, 204);
    --color-black-1: #141419;
    --color-black-2: rgb(51, 51, 51);
    --color-black-3: rgba(0, 0, 0, 0.12);
    --color-black-4: rgba(0,0,0,0.14);
    --color-black-5: rgba(0,0,0,0.12);
    --color-black-6: rgba(0,0,0,0.2);
    --color-gray-1: #999999;
    --color-gray-2: rgb(102, 102, 102);
    --color-gray-3: #ccc;

    --font-family-default: 'Roboto', sans-serif;
    --font-family-second: 'Carter One', cursive;

    --max-width-xs: calc(100% - 30px);
    --max-width-sm: 720px;
    --max-width-md: 960px;
    --max-width-lg: 1122px;
  }

  html, body, #root {
    height: 100%;
  }

  body {
    font-family: var(--font-family-default);
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    background: var(--color-primary-bg) url(${bg}) no-repeat center top;
  }

  button {
    appearance: none;
    border: none;
    outline: none;
    background: transparent;
    color: inherit;
    font: inherit;
    -webkit-font-smoothing: inherit;
    -moz-osx-font-smoothing: inherit;
    line-height: normal;
    cursor: pointer;
  }

  a {
    color: inherit;
    text-decoration: none;
    outline: none;
  }
`;

export default GlobalStyle;
