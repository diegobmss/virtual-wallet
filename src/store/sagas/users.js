import { put, takeLatest } from 'redux-saga/effects';
import { UsersTypes, UsersActions } from '../ducks/users';

function* createUserRequest({ user }) {
  try {
    const userWithCoins = {
      ...user,
      reais: 100000.00,
      bitcoin: 0,
      brita: 0,
    };

    yield put(UsersActions.createUserSuccess(userWithCoins));
  } catch (error) {
    yield put(UsersActions.createUserFailure('Falha ao cadastrar usuário. Tente novamente!'));
  }
}

function* authLoginRequest({ user }) {
  try {
    yield put(UsersActions.authLoginSuccess(user));
  } catch (error) {
    yield put(UsersActions.authLoginFailure('Falha ao efetuar o login. Tente novamente!'));
  }
}

function* buyCoinRequest({ coin, value }) {
  try {
    yield put(UsersActions.buyCoinSuccess(coin, value));
  } catch (error) {
    yield put(UsersActions.buyCoinFailure('Falha ao efetuar a compra. Tente novamente!'));
  }
}

function* sellCoinRequest({ coin, value }) {
  try {
    yield put(UsersActions.sellCoinSuccess(coin, value));
  } catch (error) {
    yield put(UsersActions.sellCoinFailure('Falha ao efetuar a venda. Tente novamente!'));
  }
}

function* changeCoinRequest({ coin_change, coin, value }) {
  try {
    yield put(UsersActions.changeCoinSuccess(coin_change, coin, value));
  } catch (error) {
    yield put(UsersActions.changeCoinFailure('Falha ao efetuar a troca. Tente novamente!'));
  }
}

export function* watchSagas() {
  yield takeLatest(UsersTypes.CREATE_USER_REQUEST, createUserRequest);
  yield takeLatest(UsersTypes.AUTH_LOGIN_REQUEST, authLoginRequest);
  yield takeLatest(UsersTypes.BUY_COIN_REQUEST, buyCoinRequest);
  yield takeLatest(UsersTypes.SELL_COIN_REQUEST, sellCoinRequest);
  yield takeLatest(UsersTypes.CHANGE_COIN_REQUEST, changeCoinRequest);
}
