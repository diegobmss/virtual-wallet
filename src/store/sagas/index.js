import { all } from 'redux-saga/effects';

import * as coins from './coins';
import * as users from './users';

function* sagas() {
  yield all([
    coins.watchSagas(),
    users.watchSagas(),
  ]);
}

export default sagas;
