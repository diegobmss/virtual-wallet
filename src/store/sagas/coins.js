import { takeLatest, put } from 'redux-saga/effects';
import axios from 'axios';
import moment from 'moment';
import { CoinsTypes, CoinsActions } from '../ducks/coins';

function* bitcoinRequest() {
  try {
    const response = yield axios.get('https://www.mercadobitcoin.net/api/BTC/ticker/');

    yield put(CoinsActions.bitcoinSuccess(response.data));
  } catch ({ response }) {
    yield put(CoinsActions.bitcoinFailure(response.data));
  }
}

function* britaRequest() {
  try {
    const date = moment().format('MM-DD-YYYY');
    const response = yield axios.get(`https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarDia(dataCotacao=@dataCotacao)?@dataCotacao='${date}'&$format=json`);

    yield put(CoinsActions.britaSuccess(response.data));
  } catch ({ response }) {
    yield put(CoinsActions.britaFailure(response.data));
  }
}

export function* watchSagas() {
  yield takeLatest(CoinsTypes.BITCOIN_REQUEST, bitcoinRequest);
  yield takeLatest(CoinsTypes.BRITA_REQUEST, britaRequest);
}
