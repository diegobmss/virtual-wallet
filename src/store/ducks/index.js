import { combineReducers } from 'redux';

import coins from './coins';
import users from './users';

export default combineReducers({
  coins,
  users,
});
