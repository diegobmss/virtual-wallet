import { createActions, createReducer } from 'reduxsauce';
import {
  REQUEST_NOT_STARTED,
  REQUEST_PENDING,
  REQUEST_RESOLVED,
  REQUEST_REJECTED,
} from '../../utils/constants';

export const { Types: CoinsTypes, Creators: CoinsActions } = createActions({
  bitcoinRequest: [],
  bitcoinSuccess: ['response'],
  bitcoinFailure: ['response'],

  britaRequest: [],
  britaSuccess: ['response'],
  britaFailure: ['response'],
});

const INITIAL_STATE = {
  bitcoin: {
    requestStatus: REQUEST_NOT_STARTED,
    requestResponse: {},
  },
  brita: {
    requestStatus: REQUEST_NOT_STARTED,
    requestResponse: {},
  },
};

const bitcoinRequest = (state) => ({
  ...state,
  bitcoin: {
    requestStatus: REQUEST_PENDING,
  },
});

const bitcoinSuccess = (state, { response }) => ({
  ...state,
  bitcoin: {
    requestResponse: response,
    requestStatus: REQUEST_RESOLVED,
  },
});

const bitcoinFailure = (state, { response }) => ({
  ...state,
  bitcoin: {
    requestResponse: response,
    requestStatus: REQUEST_REJECTED,
  },
});

const britaRequest = (state) => ({
  ...state,
  brita: {
    requestStatus: REQUEST_PENDING,
  },
});

const britaSuccess = (state, { response }) => ({
  ...state,
  brita: {
    requestResponse: response,
    requestStatus: REQUEST_RESOLVED,
  },
});

const britaFailure = (state, { response }) => ({
  ...state,
  brita: {
    requestResponse: response,
    requestStatus: REQUEST_REJECTED,
  },
});

export default createReducer(INITIAL_STATE, {
  [CoinsTypes.BITCOIN_REQUEST]: bitcoinRequest,
  [CoinsTypes.BITCOIN_SUCCESS]: bitcoinSuccess,
  [CoinsTypes.BITCOIN_FAILURE]: bitcoinFailure,
  [CoinsTypes.BRITA_REQUEST]: britaRequest,
  [CoinsTypes.BRITA_SUCCESS]: britaSuccess,
  [CoinsTypes.BRITA_FAILURE]: britaFailure,
});
