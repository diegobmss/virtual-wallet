import { createActions, createReducer } from 'reduxsauce';
import { toast } from 'react-toastify';
import moment from 'moment';

import {
  REQUEST_NOT_STARTED,
  REQUEST_PENDING,
  REQUEST_RESOLVED,
  REQUEST_REJECTED,
} from '../../utils/constants';

export const { Types: UsersTypes, Creators: UsersActions } = createActions({
  createUserRequest: ['user'],
  createUserSuccess: ['response'],
  createUserFailure: ['response'],
  cleanCreateUser: [],

  authLoginRequest: ['user'],
  authLoginSuccess: ['response'],
  authLoginFailure: ['response'],
  cleanAuthLogin: [],

  authLogoutRequest: [],

  buyCoinRequest: ['coin', 'value'],
  buyCoinSuccess: ['response'],
  buyCoinFailure: ['response'],

  sellCoinRequest: ['coin', 'value'],
  sellCoinSuccess: ['response'],
  sellCoinFailure: ['response'],

  changeCoinRequest: ['coin_change', 'coin', 'value'],
  changeCoinSuccess: ['response'],
  changeCoinFailure: ['response'],
});

const INITIAL_STATE = {
  listUsers: [],
  authenticated: {
    key: null,
    name: null,
  },
  statements: [],

  users: {
    requestStatus: REQUEST_NOT_STARTED,
  },
  auth: {
    requestStatus: REQUEST_NOT_STARTED,
  },
  buyCoin: {
    requestStatus: REQUEST_NOT_STARTED,
  },
  sellCoin: {
    requestStatus: REQUEST_NOT_STARTED,
  },
  changeCoin: {
    requestStatus: REQUEST_NOT_STARTED,
  },
};

const createUserRequest = (state) => ({
  ...state,
  users: {
    requestStatus: REQUEST_PENDING,
  },
});

const createUserSuccess = (state, { response }) => {
  const checkUserExists = state.listUsers.filter((x) => x.email === response.email);

  if (checkUserExists.length > 0) {
    return ({
      ...state,
      users: {
        requestResponse: 'Usuário já existente. Efetue o login!',
        requestStatus: REQUEST_REJECTED,
      },
    });
  }

  return ({
    ...state,
    listUsers: [...state.listUsers, response],
    users: {
      requestResponse: response,
      requestStatus: REQUEST_RESOLVED,
    },
  });
};

const createUserFailure = (state, { response }) => ({
  ...state,
  users: {
    requestResponse: response,
    requestStatus: REQUEST_REJECTED,
  },
});

const cleanCreateUser = (state) => ({
  ...state,
  users: {
    requestStatus: REQUEST_NOT_STARTED,
  },
});

const authLoginRequest = (state) => ({
  ...state,
  auth: {
    requestStatus: REQUEST_PENDING,
  },
});

const authLoginSuccess = (state, { response }) => {
  const checkUser = state.listUsers.filter((x) => x.email === response.email && x.password === response.password);
  const key = state.listUsers.indexOf(checkUser[0]);

  if (checkUser.length > 0) {
    return ({
      ...state,
      authenticated: {
        key,
        name: checkUser[0].name,
      },
      auth: {
        requestStatus: REQUEST_RESOLVED,
      },
    });
  }
  return ({
    ...state,
    authenticated: {
      key: null,
      name: null,
    },
    auth: {
      requestStatus: REQUEST_REJECTED,
      requestResponse: 'Usuário ou senha incorretos.',
    },
  });
};

const authLoginFailure = (state, { response }) => ({
  ...state,
  auth: {
    requestResponse: response,
    requestStatus: REQUEST_REJECTED,
  },
});

const cleanAuthLogin = (state) => ({
  ...state,
  auth: {
    requestStatus: REQUEST_NOT_STARTED,
  },
});

const authLogoutRequest = (state) => ({
  ...state,
  authenticated: {
    key: null,
    name: null,
  },
  auth: {
    requestStatus: REQUEST_NOT_STARTED,
  },
});

const buyCoinRequest = (state) => ({
  ...state,
  buyCoin: {
    requestStatus: REQUEST_PENDING,
  },
});

const buyCoinSuccess = (state, { response }) => {
  const value = parseFloat(response.value / response.prices[response.coin]);

  const list = state.listUsers;
  list[state.authenticated.key].reais -= response.value;
  list[state.authenticated.key][response.coin] += value;

  toast.success(`Você comprou ${value} ${response.coin}s.`);

  return ({
    ...state,
    listUsers: list,
    buyCoin: {
      requestResponse: response,
      requestStatus: REQUEST_RESOLVED,
    },
    statements: [...state.statements, {
      user: {
        ...state.authenticated,
      },
      operation: 'buy',
      coinChange: null,
      coin: response.coin,
      value,
      newValue: null,
      date: moment().format('DD/MM/YYYY'),
    }],
  });
};

const buyCoinFailure = (state, { response }) => ({
  ...state,
  buyCoin: {
    requestResponse: response,
    requestStatus: REQUEST_REJECTED,
  },
});

const sellCoinRequest = (state) => ({
  ...state,
  sellCoin: {
    requestStatus: REQUEST_PENDING,
  },
});

const sellCoinSuccess = (state, { response }) => {
  const value = parseFloat(response.value * response.prices[response.coin]);

  const list = state.listUsers;
  list[state.authenticated.key].reais += value;
  list[state.authenticated.key][response.coin] -= response.value;

  toast.success(`Você vendeu ${value} ${response.coin}s.`);

  return ({
    ...state,
    listUsers: list,
    sellCoin: {
      requestResponse: response,
      requestStatus: REQUEST_RESOLVED,
    },
    statements: [...state.statements, {
      user: {
        ...state.authenticated,
      },
      operation: 'sell',
      coinChange: null,
      coin: response.coin,
      value,
      newValue: null,
      date: moment().format('DD/MM/YYYY'),
    }],
  });
};

const sellCoinFailure = (state, { response }) => ({
  ...state,
  sellCoin: {
    requestResponse: response,
    requestStatus: REQUEST_REJECTED,
  },
});

const changeCoinRequest = (state) => ({
  ...state,
  changeCoin: {
    requestStatus: REQUEST_PENDING,
  },
});

const changeCoinSuccess = (state, { response }) => {
  const value = parseFloat(response.value * response.prices[response.coin_change]);

  const list = state.listUsers;
  list[state.authenticated.key].reais += value;
  list[state.authenticated.key][response.coin_change] -= response.value;

  const newValue = parseFloat(value / response.prices[response.coin]);

  const newList = state.listUsers;
  newList[state.authenticated.key].reais -= value;
  newList[state.authenticated.key][response.coin] += newValue;

  toast.success(`Você trocou ${value} ${response.coin_change}s por ${newValue} ${response.coin}s.`);

  return ({
    ...state,
    listUsers: list,
    changeCoin: {
      requestResponse: response,
      requestStatus: REQUEST_RESOLVED,
    },
    statements: [...state.statements, {
      user: {
        ...state.authenticated,
      },
      operation: 'change',
      coinChange: response.coin_change,
      coin: response.coin,
      value,
      newValue,
      date: moment().format('DD/MM/YYYY'),
    }],
  });
};

const changeCoinFailure = (state, { response }) => ({
  ...state,
  changeCoin: {
    requestResponse: response,
    requestStatus: REQUEST_REJECTED,
  },
});

export default createReducer(INITIAL_STATE, {
  [UsersTypes.CREATE_USER_REQUEST]: createUserRequest,
  [UsersTypes.CREATE_USER_SUCCESS]: createUserSuccess,
  [UsersTypes.CREATE_USER_FAILURE]: createUserFailure,
  [UsersTypes.CLEAN_CREATE_USER]: cleanCreateUser,

  [UsersTypes.AUTH_LOGIN_REQUEST]: authLoginRequest,
  [UsersTypes.AUTH_LOGIN_SUCCESS]: authLoginSuccess,
  [UsersTypes.AUTH_LOGIN_FAILURE]: authLoginFailure,
  [UsersTypes.CLEAN_AUTH_LOGIN]: cleanAuthLogin,

  [UsersTypes.AUTH_LOGOUT_REQUEST]: authLogoutRequest,

  [UsersTypes.BUY_COIN_REQUEST]: buyCoinRequest,
  [UsersTypes.BUY_COIN_SUCCESS]: buyCoinSuccess,
  [UsersTypes.BUY_COIN_FAILURE]: buyCoinFailure,

  [UsersTypes.SELL_COIN_REQUEST]: sellCoinRequest,
  [UsersTypes.SELL_COIN_SUCCESS]: sellCoinSuccess,
  [UsersTypes.SELL_COIN_FAILURE]: sellCoinFailure,

  [UsersTypes.CHANGE_COIN_REQUEST]: changeCoinRequest,
  [UsersTypes.CHANGE_COIN_SUCCESS]: changeCoinSuccess,
  [UsersTypes.CHANGE_COIN_FAILURE]: changeCoinFailure,
});
