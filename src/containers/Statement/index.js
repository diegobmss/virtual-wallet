import React from 'react';

import Statement from '../../components/presentation/Operations/Statement';

const StatementContainer = () => <Statement />;

export default StatementContainer;
