import React from 'react';

import Home from '../../components/presentation/Home';

const HomeContainer = () => <Home />;

export default HomeContainer;
