import React from 'react';

import SignOut from '../../components/presentation/Auth/SignOut';

const SignOutContainer = () => <SignOut />;

export default SignOutContainer;
