import React from 'react';

import SignIn from '../../components/presentation/Auth/SignIn';

const SignInContainer = () => <SignIn />;

export default SignInContainer;
