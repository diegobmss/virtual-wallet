import React from 'react';

import Change from '../../components/presentation/Operations/Change';

const ChangeContainer = () => <Change />;

export default ChangeContainer;
