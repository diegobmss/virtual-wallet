import React from 'react';

import SignUp from '../../components/presentation/Auth/SignUp';

const SignUpContainer = () => <SignUp />;

export default SignUpContainer;
