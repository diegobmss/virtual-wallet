import React from 'react';
import { ToastContainer } from 'react-toastify';
import { BrowserRouter as Router } from 'react-router-dom';

import GlobalStyle from './globalStyle';
import Routes from './routes';

const App = () => (
  <Router>
    <GlobalStyle />
    <Routes />
    <ToastContainer autoClose={3000} />
  </Router>
);

export default App;
