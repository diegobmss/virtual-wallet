import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { useSelector } from 'react-redux';

import Header from '../components/presentation/Header';

import Buy from '../containers/Buy';
import Change from '../containers/Change';
import Home from '../containers/Home';
import Sell from '../containers/Sell';
import SignIn from '../containers/SignIn';
import SignOut from '../containers/SignOut';
import SignUp from '../containers/SignUp';
import Statement from '../containers/Statement';

const PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (isAuthenticated ? (
      <>
        <Header />
        <Component {...props} />
      </>
    ) : <Redirect to={{ pathname: '/login' }} />)}
  />
);

const Routes = () => {
  const { authenticated } = useSelector((state) => state.users);

  const isAuthenticated = authenticated.name;

  return (
    <Switch>
      <Route
        path="/login"
        render={() => (isAuthenticated ? (
          <Redirect to="/" />
        ) : (
          <Route exact path="/login" component={SignIn} />
        ))}
      />
      <Route exact path="/register" component={SignUp} />
      <PrivateRoute exact path="/" component={Home} isAuthenticated={isAuthenticated} />
      <PrivateRoute exact path="/buy" component={Buy} isAuthenticated={isAuthenticated} />
      <PrivateRoute exact path="/change" component={Change} isAuthenticated={isAuthenticated} />
      <PrivateRoute exact path="/sell" component={Sell} isAuthenticated={isAuthenticated} />
      <PrivateRoute exact path="/statement" component={Statement} isAuthenticated={isAuthenticated} />
      <PrivateRoute exact path="/logout" component={SignOut} isAuthenticated={isAuthenticated} />

      {/* <Route exact path="/login" component={SignIn} />
      <Route exact path="/register" component={SignUp} />
      <Route exact path='/' component={Home} /> */}
      <Route path="*">
        <Redirect to="/" />
      </Route>
    </Switch>
  );
};

export default Routes;
